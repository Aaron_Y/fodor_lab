/**Functions for serverside js for BioLockJ guide
  *Author: Aaron Yerke
  *
*/

var fs = require('fs'),
  path = require('path');


/**parseBljJson turns a config json into a string in flat file format.
*/
exports.parseBljJson = function(currentConfig){
  var text = "";
  textFile = null;
  //add modules to config first
  if (currentConfig["modules"] != null){
    for (var i = 0; i < currentConfig["modules"].length; i++) {
      text += currentConfig["modules"][i].concat("\n");
    }
  };
  //for non_module
  for (var key in currentConfig) {
    //console.log(key);
    if (currentConfig.hasOwnProperty(key)) { //only lets keys that are user inputed pass
      if (key == "modules" || key == "project.configFile") {// skipping project.configFile and modules
      } else if (currentConfig[key] != '') { //project.configFile doesn't go inside the document
        text += key.concat("=", currentConfig[key], "\n");
      }
    }
  }
  return text;
};

exports.progressStatus = function(dirPath){
  fs.watch(dirPath, (eventType, filename) => {
    console.log(`Filename: ${filename}, Event: ${eventType}`);
    console.log(fs.lstatSync('/Users/aaronyerke/Desktop/fodor_lab/blj_testing/' + filename).isDirectory());
  // could be either 'rename' or 'change'. new file event and delete
  // also generally emit 'rename'
  })
}

exports.newestFileInDir = function(dirPath){

}



//shamelessly ripped from https://stackoverflow.com/questions/2727167/how-do-you-get-a-list-of-the-names-of-all-files-present-in-a-directory-in-node-j
// async version with basic error handling
exports.mapDir = function (currentDirPath, callback) {
    fs.readdir(currentDirPath, function (err, files) {
        if (err) {
            throw new Error(err);
        }
        files.forEach(function (name) {
            var filePath = path.join(currentDirPath, name);
            var stat = fs.statSync(filePath);
            if (stat.isFile()) {
                callback(filePath, stat);
            } else if (stat.isDirectory()) {
                walk(filePath, callback);
            }
        });
    });
}
/*use with line like:
    walk('path/to/root/dir', function(filePath, stat) {
        // do something with "filePath"...
    });
*/
// fs.readdir('/Users/aaronyerke/Desktop/fodor_lab/blj_testing', (err, files) => {
//   files.forEach(file => {
//     console.log(file);
//   });
// })

exports.testing = function(currentConfig){
  console.log('asfdsdklfjadslfjsadlk;fjsdl;kfjasdlfkjaf;asldfjasdlkfjas;dfjasd;lfjkasd;f');
}
